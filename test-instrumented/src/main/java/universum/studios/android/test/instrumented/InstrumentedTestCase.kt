/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.test.instrumented

import android.app.Instrumentation
import android.content.Context
import androidx.annotation.CallSuper
import androidx.annotation.NonNull
import androidx.annotation.WorkerThread
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith

/**
 * Class that may be used to group suite of **Android instrumented tests**.
 *
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4::class)
abstract class InstrumentedTestCase {

    /**
     */
    companion object {

        /**
         * Delegates to [Instrumentation.waitForIdleSync].
         */
        @WorkerThread protected fun waitForIdleSync() = InstrumentationRegistry.getInstrumentation().waitForIdleSync()
    }

    /**
     * Application context instance obtained via [ApplicationProvider.getApplicationContext].
     *
     * It is always valid between calls to [beforeTest] and [afterTest].
     */
    @NonNull lateinit var context: Context

    /**
     * Called before execution of each test method starts.
     */
    @Before @CallSuper fun beforeTest() {
        // Inheritance hierarchies may for example acquire here resources needed for each test.
        this.context = ApplicationProvider.getApplicationContext()
    }

    /**
     * Called after execution of each test method finishes.
     */
    @After @CallSuper fun afterTest() {
        // Inheritance hierarchies may for example release here resources acquired in beforeTest() call.
    }
}