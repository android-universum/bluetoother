/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.test.local

import android.app.Application
import android.content.Context
import androidx.annotation.CallSuper
import androidx.test.core.app.ApplicationProvider
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

/**
 * Class that may be used to group **suite of Android tests** to be executed on a local *JVM*
 * with shadowed *Android environment* using [RobolectricTestRunner].
 *
 * @author Martin Albedinsky
 */
@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner::class)
abstract class RobolectricTestCase : LocalTestCase() {

    /**
     * Context instance obtained via [ApplicationProvider.getApplicationContext] as [Application].
     *
     * It is always valid between calls to [beforeTest] and [afterTest].
     */
    protected val application: Application by lazy { ApplicationProvider.getApplicationContext<Application>() }

    /**
     * Context instance obtained via [ApplicationProvider.getApplicationContext] as [Context].
     *
     * It is always valid between calls to [beforeTest] and [afterTest].
     */
    protected val context: Context by lazy { ApplicationProvider.getApplicationContext<Context>() }

    /*
     */
    @CallSuper override fun beforeTest() = super.beforeTest()

    /*
     */
    @CallSuper override fun afterTest() = super.afterTest()
}