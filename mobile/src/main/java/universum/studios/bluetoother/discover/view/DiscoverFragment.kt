/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.bluetoother.discover.view

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_discover.*
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import universum.studios.android.fragment.annotation.ContentView
import universum.studios.bluetoother.R
import universum.studios.bluetoother.discover.control.DiscoverController
import universum.studios.bluetoother.discover.view.presentation.DiscoveredDevicesAdapter
import universum.studios.bluetoother.view.BaseFragment

/**
 * @author Martin Albedinsky
 */
@RuntimePermissions
@ContentView(R.layout.fragment_discover)
class DiscoverFragment : BaseFragment<DiscoverViewModel, DiscoverController>(), DiscoverView {

    private var adapter: DiscoveredDevicesAdapter? = null

    override fun onAttach(context: Context?) {
        requestFeature(FEATURE_INJECTION_BASIC)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.adapter = DiscoveredDevicesAdapter(requireContext())
    }

    override fun onBindViews(rootView: View, savedInstanceState: Bundle?) {
        super.onBindViews(rootView, savedInstanceState)
        this.list.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        startScanWithPermissionsCheck()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // todo: DiscoverFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    private fun startScanWithPermissionsCheck() {
        startScan()
    }

    @NeedsPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    fun startScan() {
        getController().startScan()
    }

    override fun onStop() {
        super.onStop()
        getController().stopScan()
    }
}