/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.bluetoother.discover.view.presentation

import android.bluetooth.BluetoothDevice
import universum.studios.android.arkhitekton.view.presentation.BasePresenter
import universum.studios.bluetoother.discover.view.DiscoverView
import universum.studios.bluetoother.discover.view.DiscoverViewModel

/**
 * @author Martin Albedinsky
 */
class DefaultDiscoverPresenter private constructor(builder: BaseBuilder<*, DiscoverView, DiscoverViewModel>)
	: BasePresenter<DiscoverView, DiscoverViewModel>(builder), DiscoverPresenter {

	companion object {

		fun create(viewModel: DiscoverViewModel) = DefaultDiscoverPresenter(SimpleBuilder.create(viewModel))
	}

	override fun onScanStarted() {
	}

	override fun onDevicesFound(devices: List<BluetoothDevice>) {
		getViewModel().updateDevices(devices)
	}

	override fun onScanFinished() {
	}
}