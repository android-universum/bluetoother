/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.bluetoother.discover.view

import android.bluetooth.BluetoothManager
import android.content.Context
import dagger.Module
import dagger.Provides
import universum.studios.android.arkhitekton.interaction.Interactor
import universum.studios.bluetoother.ApplicationContext
import universum.studios.bluetoother.discover.control.DefaultDiscoverController
import universum.studios.bluetoother.discover.control.DiscoverController
import universum.studios.bluetoother.discover.view.presentation.DefaultDiscoverPresenter
import universum.studios.bluetoother.discover.view.presentation.DiscoverPresenter
import universum.studios.bluetoother.view.BaseFragmentModule
import universum.studios.bluetoother.view.EmptyFragmentModule

/**
 * @author Martin Albedinsky
 */
@Module(includes = [EmptyFragmentModule::class])
class DiscoverFragmentModule : BaseFragmentModule() {

	@Provides protected fun provideController(
            fragment: DiscoverFragment,
            interactor: Interactor,
            presenter: DiscoverPresenter,
            @ApplicationContext context: Context
	): DiscoverController {
	    val holder = provideControllerHolder(fragment, DiscoverController.Holder::class.java)
		return if (holder.hasController()) holder.getController()
        else holder.attachController(DefaultDiscoverController.Builder()
                .apply {
					this.interactor = interactor
					this.presenter = presenter
                    this.context = context
                    this.bluetoothAdapter = (context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
                }
                .build())
	}
	
	@Provides protected fun providePresenter(viewModel: DiscoverViewModel): DiscoverPresenter = DefaultDiscoverPresenter.create(viewModel)
	@Provides protected fun provideViewModel(fragment: DiscoverFragment): DiscoverViewModel = provideViewModel(fragment, DiscoverViewModelImpl::class.java)
}