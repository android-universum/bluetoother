/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.bluetoother.discover.view.presentation

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.View
import android.view.ViewGroup
import universum.studios.android.widget.adapter.SimpleRecyclerAdapter
import universum.studios.android.widget.adapter.holder.RecyclerViewHolder

/**
 * @author Martin Albedinsky
 */
class DiscoveredDevicesAdapter(context: Context) : SimpleRecyclerAdapter<DiscoveredDevicesAdapter, DiscoveredDevicesAdapter.ItemHolder, BluetoothDevice>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return super.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(viewHolder: ItemHolder, position: Int) {
        super.onBindViewHolder(viewHolder, position)
    }

    inner class ItemHolder(itemView: View) : RecyclerViewHolder(itemView) {


    }
}