/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.bluetoother

import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import universum.studios.bluetoother.discover.view.DiscoverActivity
import universum.studios.bluetoother.discover.view.DiscoverActivityModule

/**
 * @author Martin Albedinsky
 */
@Module(includes = [AndroidInjectionModule::class, AndroidSupportInjectionModule::class])
internal abstract class ActivityInjectionModule {

    @ContributesAndroidInjector(modules = [DiscoverActivityModule::class])
    internal abstract fun contributeDiscoverActivityInjector(): DiscoverActivity
}