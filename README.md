Bluetoother
===========

[![Kotlin](https://img.shields.io/badge/kotlin-1.3.20-blue.svg)](https://kotlinlang.org)
[![Android](https://img.shields.io/badge/android-9.0-blue.svg)](https://developer.android.com/about/versions/pie/android-9.0)
[![Robolectric](https://img.shields.io/badge/robolectric-4.1-blue.svg)](http://robolectric.org)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

## [License](https://bitbucket.org/android-universum/bluetoother/src/master/LICENSE.md) ##

**Copyright 2018 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.